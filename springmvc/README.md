## Update to run standalone tomca7
- To run standalone with `tomcat7-maven-plugin` please use command with specific version of tomcat:
```
mvn clean install tomcat7:run
```